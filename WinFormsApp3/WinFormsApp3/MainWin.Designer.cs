﻿namespace WinFormsApp3
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            colorDialog1 = new ColorDialog();
            figure1 = new Figure();
            figure2 = new Figure();
            SuspendLayout();
            // 
            // figure1
            // 
            figure1.Font = new Font("Segoe UI", 24F, FontStyle.Regular, GraphicsUnit.Point, 0);
            figure1.Location = new Point(57, 62);
            figure1.Margin = new Padding(8, 9, 8, 9);
            figure1.Name = "figure1";
            figure1.Size = new Size(100, 100);
            figure1.TabIndex = 0;
            // 
            // figure2
            // 
            figure2.Font = new Font("Segoe UI", 24F, FontStyle.Regular, GraphicsUnit.Point, 0);
            figure2.Location = new Point(260, 126);
            figure2.Margin = new Padding(8, 9, 8, 9);
            figure2.Name = "figure2";
            figure2.Size = new Size(100, 100);
            figure2.TabIndex = 1;
            // 
            // MainWindow
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(figure2);
            Controls.Add(figure1);
            Name = "MainWindow";
            Text = "Form1";
            MouseDown += MainWindow_MouseDown;
            ResumeLayout(false);
        }

        #endregion

        private ColorDialog colorDialog1;
        private Figure figure1;
        private Figure figure2;
    }
}

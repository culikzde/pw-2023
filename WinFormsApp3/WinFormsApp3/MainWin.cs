using System.Drawing;
using System.Windows.Forms;

namespace WinFormsApp3
{
    public partial class MainWindow : Form
    {
        private const int size = 100;
        private const int N = 8;
        private Panel[,] pole = new Panel[N, N];

        private void figurka(int i, int k, string s)
        {
            Button b = new Button();
            b.Text = s;
            b.Font = new Font("", 48);
            b.Size = new Size(size, size);
            b.Location = new Point(k * size, i * size);
            b.Parent = this;
            // b.BringToFront
            // vez.Parent = pole[i,k];
            // pole[i, k].Controls.Add(b);

            b.MouseDown += figurka_MouseDown;
            b.MouseMove += figurka_MouseMove;
            b.MouseUp += figurka_MouseUp;
        }

        private int X0, Y0;
        private bool down = false;

        private void figurka_MouseDown (object sender, MouseEventArgs e)
        {
            // Text = "Mouse Down";
            Button b = sender as Button;
            // b.BackColor = Color.Green;
            X0 = e.X;
            Y0 = e.Y;
            down = true;

            // b.BringToFront();
            if (e.Button == MouseButtons.Middle || Control.ModifierKeys == Keys.Shift)
            {
                colorDialog1.Color = b.BackColor;
                if (colorDialog1.ShowDialog() == DialogResult.OK)
                {
                    b.BackColor = colorDialog1.Color;
                }
            }
        }

        private void figurka_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                Button b = sender as Button;
                if (e.Button == MouseButtons.Right || Control.ModifierKeys == Keys.Control)
                {
                    // resize
                    b.Width += e.X - X0;
                    b.Height += e.Y - Y0;
                    X0 = e.X;
                    Y0 = e.Y;
                }
                else
                {
                    // move
                    b.Left += e.X - X0;
                    b.Top += e.Y - Y0;
                }
            }
        }

        private void figurka_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;
        }

        public MainWindow()
        {
            InitializeComponent();
            Size = new Size(N * size + 40, N * size + 40);

            if (false)
            for (int i = 0; i < N; i++)
                for (int k = 0; k < N; k++)
                {
                    Panel p = new Panel();
                    pole[i, k] = p;
                    p.BackColor = (i + k) % 2 == 0 ? Color.Yellow : Color.CornflowerBlue;
                    // p.BorderStyle = BorderStyle.Fixed3D;
                    // p.BorderStyle = BorderStyle.FixedSingle;
                    // p.BorderStyle = BorderStyle.None;
                    p.Location = new Point(k * size, i * size);
                    // p.Name = "a8";
                    p.Size = new Size(size, size);
                    p.Parent = this;
                    /* this. */ /* Controls.Add (p); */
                }

            string[] cerne = { "\u265c", "\u265e", "\u265d", "\u265b",
                               "\u265a", "\u265d", "\u265e", "\u265c"};

            string[] bile = { "\u2656", "\u2658", "\u2657", "\u2655",
                               "\u2654", "\u2657", "\u2658", "\u2656"};

            const string bily_pesec = "\u265f";
            const string cerny_pesec = "\u2659";

            for (int k = 0; k < N; k++)
                figurka(0, k, cerne[k]);

            for (int k = 0; k < N; k++)
                figurka(1, k, bily_pesec);

            for (int k = 0; k < N; k++)
                figurka(6, k, cerny_pesec);

            for (int k = 0; k < N; k++)
                figurka(7, k, bile[k]);
        }

        private void test_panel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MainWindow_MouseDown(object sender, MouseEventArgs e)
        {
        }

    }
}

﻿namespace Parameters
{
    partial class Window
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            menuStrip1 = new MenuStrip();
            fileToolStripMenuItem = new ToolStripMenuItem();
            openMenuItem = new ToolStripMenuItem();
            saveMenuItem = new ToolStripMenuItem();
            menuSeparator1 = new ToolStripSeparator();
            readMenuItem = new ToolStripMenuItem();
            writeMenuItem = new ToolStripMenuItem();
            menuSeparator2 = new ToolStripSeparator();
            quitMenuItem = new ToolStripMenuItem();
            splitContainer1 = new SplitContainer();
            tree = new TreeView();
            splitContainer2 = new SplitContainer();
            grid = new DataGridView();
            splitContainer3 = new SplitContainer();
            panel = new Panel();
            horizSplitContainer = new SplitContainer();
            edit = new TextBox();
            propGrid = new PropertyGrid();
            NameColumn = new DataGridViewTextBoxColumn();
            ValueColumn = new DataGridViewTextBoxColumn();
            menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer2).BeginInit();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)grid).BeginInit();
            ((System.ComponentModel.ISupportInitialize)splitContainer3).BeginInit();
            splitContainer3.Panel1.SuspendLayout();
            splitContainer3.Panel2.SuspendLayout();
            splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)horizSplitContainer).BeginInit();
            horizSplitContainer.Panel2.SuspendLayout();
            horizSplitContainer.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { fileToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1175, 24);
            menuStrip1.TabIndex = 3;
            menuStrip1.Text = "fileMenu";
            // 
            // fileToolStripMenuItem
            // 
            fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { openMenuItem, saveMenuItem, menuSeparator1, readMenuItem, writeMenuItem, menuSeparator2, quitMenuItem });
            fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            fileToolStripMenuItem.Size = new Size(37, 20);
            fileToolStripMenuItem.Text = "&File";
            // 
            // openMenuItem
            // 
            openMenuItem.Name = "openMenuItem";
            openMenuItem.ShortcutKeys = Keys.Control | Keys.O;
            openMenuItem.Size = new Size(172, 22);
            openMenuItem.Text = "&Open";
            openMenuItem.Click += openMenuItem_Click;
            // 
            // saveMenuItem
            // 
            saveMenuItem.Name = "saveMenuItem";
            saveMenuItem.ShortcutKeys = Keys.Control | Keys.S;
            saveMenuItem.Size = new Size(172, 22);
            saveMenuItem.Text = "&Save";
            saveMenuItem.Click += saveMenuItem_Click;
            // 
            // menuSeparator1
            // 
            menuSeparator1.Name = "menuSeparator1";
            menuSeparator1.Size = new Size(169, 6);
            // 
            // readMenuItem
            // 
            readMenuItem.Name = "readMenuItem";
            readMenuItem.ShortcutKeys = Keys.F3;
            readMenuItem.Size = new Size(172, 22);
            readMenuItem.Text = "&Read XML Text";
            readMenuItem.Click += readMenuItem_Click;
            // 
            // writeMenuItem
            // 
            writeMenuItem.Name = "writeMenuItem";
            writeMenuItem.ShortcutKeys = Keys.F2;
            writeMenuItem.Size = new Size(172, 22);
            writeMenuItem.Text = "Write XML Text";
            writeMenuItem.Click += writeMenuItem_Click;
            // 
            // menuSeparator2
            // 
            menuSeparator2.Name = "menuSeparator2";
            menuSeparator2.Size = new Size(169, 6);
            // 
            // quitMenuItem
            // 
            quitMenuItem.Name = "quitMenuItem";
            quitMenuItem.ShortcutKeys = Keys.Control | Keys.Q;
            quitMenuItem.Size = new Size(172, 22);
            quitMenuItem.Text = "&Quit";
            quitMenuItem.Click += quitMenuItem_Click;
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(0, 24);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(tree);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(splitContainer2);
            splitContainer1.Size = new Size(1175, 588);
            splitContainer1.SplitterDistance = 391;
            splitContainer1.TabIndex = 4;
            // 
            // tree
            // 
            tree.Dock = DockStyle.Fill;
            tree.Location = new Point(0, 0);
            tree.Name = "tree";
            tree.Size = new Size(391, 588);
            tree.TabIndex = 0;
            tree.AfterSelect += tree_AfterSelect;
            // 
            // splitContainer2
            // 
            splitContainer2.Dock = DockStyle.Fill;
            splitContainer2.Location = new Point(0, 0);
            splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            splitContainer2.Panel1.Controls.Add(grid);
            // 
            // splitContainer2.Panel2
            // 
            splitContainer2.Panel2.Controls.Add(splitContainer3);
            splitContainer2.Size = new Size(780, 588);
            splitContainer2.SplitterDistance = 352;
            splitContainer2.TabIndex = 0;
            // 
            // grid
            // 
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grid.Columns.AddRange(new DataGridViewColumn[] { NameColumn, ValueColumn });
            grid.Dock = DockStyle.Fill;
            grid.Location = new Point(0, 0);
            grid.Name = "grid";
            grid.RowTemplate.Height = 25;
            grid.Size = new Size(352, 588);
            grid.TabIndex = 0;
            // 
            // splitContainer3
            // 
            splitContainer3.Dock = DockStyle.Fill;
            splitContainer3.Location = new Point(0, 0);
            splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            splitContainer3.Panel1.Controls.Add(panel);
            splitContainer3.Panel1.Controls.Add(horizSplitContainer);
            // 
            // splitContainer3.Panel2
            // 
            splitContainer3.Panel2.Controls.Add(propGrid);
            splitContainer3.Size = new Size(424, 588);
            splitContainer3.SplitterDistance = 141;
            splitContainer3.TabIndex = 2;
            // 
            // panel
            // 
            panel.BorderStyle = BorderStyle.Fixed3D;
            panel.Dock = DockStyle.Fill;
            panel.Location = new Point(0, 0);
            panel.Name = "panel";
            panel.Size = new Size(141, 588);
            panel.TabIndex = 3;
            // 
            // horizSplitContainer
            // 
            horizSplitContainer.Dock = DockStyle.Fill;
            horizSplitContainer.Location = new Point(0, 0);
            horizSplitContainer.Name = "horizSplitContainer";
            horizSplitContainer.Orientation = Orientation.Horizontal;
            // 
            // horizSplitContainer.Panel2
            // 
            horizSplitContainer.Panel2.Controls.Add(edit);
            horizSplitContainer.Size = new Size(141, 588);
            horizSplitContainer.SplitterDistance = 112;
            horizSplitContainer.TabIndex = 5;
            // 
            // edit
            // 
            edit.Dock = DockStyle.Fill;
            edit.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            edit.Location = new Point(0, 0);
            edit.Multiline = true;
            edit.Name = "edit";
            edit.Size = new Size(141, 472);
            edit.TabIndex = 5;
            edit.WordWrap = false;
            // 
            // propGrid
            // 
            propGrid.Dock = DockStyle.Fill;
            propGrid.Location = new Point(0, 0);
            propGrid.Name = "propGrid";
            propGrid.Size = new Size(279, 588);
            propGrid.TabIndex = 2;
            // 
            // NameColumn
            // 
            NameColumn.HeaderText = "Name";
            NameColumn.Name = "NameColumn";
            // 
            // ValueColumn
            // 
            ValueColumn.HeaderText = "Value";
            ValueColumn.Name = "ValueColumn";
            // 
            // Window
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1175, 612);
            Controls.Add(splitContainer1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "Window";
            Text = "Properties";
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer2).EndInit();
            splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)grid).EndInit();
            splitContainer3.Panel1.ResumeLayout(false);
            splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer3).EndInit();
            splitContainer3.ResumeLayout(false);
            horizSplitContainer.Panel2.ResumeLayout(false);
            horizSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)horizSplitContainer).EndInit();
            horizSplitContainer.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem readMenuItem;
        private ToolStripMenuItem writeMenuItem;
        private ToolStripMenuItem openMenuItem;
        private ToolStripMenuItem saveMenuItem;
        private ToolStripSeparator menuSeparator1;
        private ToolStripSeparator menuSeparator2;
        private ToolStripMenuItem quitMenuItem;
        private SplitContainer splitContainer1;
        private TreeView tree;
        private SplitContainer splitContainer2;
        private SplitContainer splitContainer3;
        private DataGridView grid;
        private Panel panel;
        private SplitContainer horizSplitContainer;
        private PropertyGrid propGrid;
        private TextBox edit;
        private DataGridViewTextBoxColumn NameColumn;
        private DataGridViewTextBoxColumn ValueColumn;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Numerics;
using System.Transactions;

namespace Parameters
{
    public class BrushProperties
    {
        private Panel panel;

        public BrushProperties (Panel p)
        {
            cnt++;

            Cisla = new List<int> { };
            Texty = new List<string> { };

            panel = p;

            Color1 = Color.Lime;
            Color2 = Color.Yellow;

            X1 = 0;
            Y1 = 0;

            X2 = 100;
            Y2 = 100;

            cnt--;
            refresh ();
        }
        public BrushProperties ( ) : 
            this (null)
        {
            // XML vyzaduje konstruktor bez parametru
            // a take public class
        }

         public Brush createBrush()
        {
            if (X1 == X2 && Y1 == Y2)
                return new SolidBrush (Color1);
            else 
                return new LinearGradientBrush (new PointF(X1, Y1),
                                                new PointF(X2, Y2),
                                                Color1,
                                                Color2);
        }

        int cnt = 0;
        private void refresh ()
        {
            if (cnt == 0 && panel != null)
            {
                cnt++;

                Brush brush = createBrush();

                int w = panel.Width;
                int h = panel.Height;
                Bitmap bitmap = new Bitmap(w, h);
                Graphics g = Graphics.FromImage(bitmap);
                g.FillEllipse(brush, 10, 10, w-20, h-20);

                panel.BackgroundImage = bitmap;

                cnt--;
            }
        }

        // public String Text { get; set;  }

        private Color color1;

        [Category("Colors")]
        [DisplayName("FirstColor")]
        [Description("First Color ...")]
        public Color Color1 
        { 
            get { return color1; } 
            set { color1 = value; refresh (); } 
        }

        private Color color2;

        [Category("Colors")]
        [DisplayName("SecondColor")]
        [Description("Second Color ...")]
        public Color Color2
        {
            get => color2; 
            set { color2 = value; refresh(); } 
        }

        int x1, y1, x2, y2;

        [Category("From")]
        public int X1 { get => x1; set { x1 = value; refresh(); } }
        
        [Category("From")]
        public int Y1 { get => y1; set { y1 = value; refresh(); } }

        [Category("To")]
        public int X2 { get => x2; set { x2 = value; refresh(); } }

        [Category("To")]
        public int Y2 { get => y2; set { y2 = value; refresh(); } }

        [Category("Example")]
        public List<int> Cisla { get; set; }
        [Category("Example")]
        public List<string> Texty { get; set; }

        public enum SizeType { small, medium, large };

        [Category("Example")]
        public SizeType Size { get; set; }

        private void init ()
        {
        }
    }
}

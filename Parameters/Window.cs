
using System.Xml.Serialization;
using System.IO;
using System.Threading.Tasks.Dataflow;
using System.ComponentModel;
using System.Reflection;

namespace Parameters
{
    public partial class Window : Form
    {
        [Description ("Param")]
        BrushProperties param;

        public Window()
        {
            InitializeComponent();

            param = new BrushProperties(panel);
            param.Cisla = new List<int> { 10, 20, 30 };
            param.Texty = new List<string> { "a", "bb", "ccc" };
            propGrid.SelectedObject = param;

            // displayStructure(param);
            displayStructure (this);
        }

        private void setParameters(BrushProperties input)
        {
            // zkopirovat prectene parametry
            param.X1 = input.X1;
            param.Y1 = input.Y1;
            param.X2 = input.X2;
            param.Y2 = input.Y2;

            param.Cisla = input.Cisla;
            param.Texty = input.Texty;
            param.Size = input.Size;

            // znovu zobrazit
            propGrid.SelectedObject = param;
        }

        [Description("Open")]
        private void openMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                XmlSerializer r = new XmlSerializer(typeof(BrushProperties));

                using (StreamReader stream = new StreamReader(dialog.FileName))
                {
                    BrushProperties input = r.Deserialize(stream) as BrushProperties;
                    setParameters(input);
                }
            }

        }

        [Description("Save")]
        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                XmlSerializer w = new XmlSerializer(typeof(BrushProperties));
                using (StreamWriter stream = new StreamWriter(dialog.FileName))
                {
                    w.Serialize(stream, param);
                }
            }
        }

        [Description("Read")]
        private void readMenuItem_Click(object sender, EventArgs e)
        {
            XmlSerializer r = new XmlSerializer(typeof(BrushProperties));
            StringReader stream = new StringReader(edit.Text);
            BrushProperties input = r.Deserialize(stream) as BrushProperties;
            stream.Close();
            setParameters(input);
        }

        [Description("Write")]
        private void writeMenuItem_Click(object sender, EventArgs e)
        {
            XmlSerializer w = new XmlSerializer(typeof(BrushProperties));
            StringWriter stream = new StringWriter();
            w.Serialize(stream, param);
            stream.Close();
            edit.Text = stream.ToString();
        }

        [Description ("Quit")]
        private void quitMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void displayStructure(object obj)
        {
            addStructure(tree.Nodes, obj);
        }

        private void addStructure(TreeNodeCollection target, object obj)
        {
            TreeNode node = new TreeNode();
            node.Tag = obj;
            string typ = obj.GetType().ToString();

            if (obj is Control)
            {
                Control c = obj as Control;
                node.Text = c.Name + " : " + typ;

                foreach (Control item in c.Controls)
                    addStructure(node.Nodes, item);
            }
            else
            {
                node.Text = obj.ToString() + " : " + typ;
            }

            addAttributes(node.Nodes, obj);

            target.Add(node);
        }
        private void addAttributes(TreeNodeCollection target, object obj)
        {
            Type type = obj.GetType();

            /*
            foreach (Attribute attr in type.GetCustomAttributes ())
            {
                TreeNode node = new TreeNode ();
                node.Tag = attr;
                node.Text = attr.ToString ();
                node.ForeColor = Color.CornflowerBlue;
                target.Add (node);
            }
            */

            foreach (DescriptionAttribute attr in type.GetCustomAttributes<DescriptionAttribute>())
            {
                TreeNode node = new TreeNode();
                node.Tag = attr;
                node.Text = attr.Description;
                node.ForeColor = Color.CornflowerBlue;
                target.Add(node);
            }

            /*
            foreach (MethodInfo meth in type.GetRuntimeMethods ())
            {
                if (meth.DeclaringType == type)
                {
                    TreeNode node = new TreeNode ();
                    node.Tag = meth;
                    node.Text = meth.Name;
                    node.ForeColor = Color.Orange;
                    target.Add (node);
                }
            }
            */

            /*
            foreach (MethodInfo meth in type.GetMethods ())
            {
                foreach (Attribute attr in meth.GetCustomAttributes ())
                {
                    TreeNode node = new TreeNode ();
                    node.Tag = attr;
                    node.Text = meth.Name + " : " + attr.ToString ();
                    node.ForeColor = Color.Orange;
                    target.Add (node);
                }
            }
            */

            foreach (MethodInfo meth in type.GetRuntimeMethods())
            {
                foreach (DescriptionAttribute attr in meth.GetCustomAttributes<DescriptionAttribute>())
                {
                    TreeNode node = new TreeNode();
                    node.Tag = attr;
                    node.Text = meth.Name + " : " + attr.Description;
                    node.ForeColor = Color.Orange;
                    target.Add(node);
                }
            }

            foreach (FieldInfo field in type.GetRuntimeFields())
            {
                var list = field.GetCustomAttributes<DescriptionAttribute>();
                foreach (DescriptionAttribute attr in list)
                {
                    TreeNode node = new TreeNode();
                    node.Tag = attr;
                    node.Text = field.Name + " : " + attr.Description;
                    node.ForeColor = Color.Lime;
                    target.Add(node);
                }
            }
        }

        private void tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            object obj = e.Node.Tag;
            Type typ = obj.GetType();

            propGrid.SelectedObject = obj;
            // typeGrid.SelectedObject = obj.GetType().GetTypeInfo();

            foreach (PropertyInfo prop in typ.GetProperties())
            {
                string name = prop.Name;
                object value = null;
                if (prop.CanRead)
                {
                    value = prop.GetValue(obj, null); /* second parameter required from .Net 4.5 */
                }
                object[] line = new object[] { name, value };
                grid.Rows.Add(line);
            }
        }
    }
}

﻿namespace Paint2
{
    partial class Box
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            contextMenu = new ContextMenuStrip(components);
            openMenu = new ToolStripMenuItem();
            resizeMenu = new ToolStripMenuItem();
            contextMenu.SuspendLayout();
            SuspendLayout();
            // 
            // contextMenu
            // 
            contextMenu.Items.AddRange(new ToolStripItem[] { openMenu, resizeMenu });
            contextMenu.Name = "contextMenu";
            contextMenu.Size = new Size(181, 70);
            // 
            // openMenu
            // 
            openMenu.Name = "openMenu";
            openMenu.Size = new Size(180, 22);
            openMenu.Text = "&Open";
            openMenu.Click += openMenu_Click;
            // 
            // resizeMenu
            // 
            resizeMenu.Name = "resizeMenu";
            resizeMenu.Size = new Size(180, 22);
            resizeMenu.Text = "&Resize";
            resizeMenu.Click += resizeMenu_Click;
            // 
            // Box
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.CornflowerBlue;
            ContextMenuStrip = contextMenu;
            Name = "Box";
            Size = new Size(280, 136);
            MouseDown += Box_MouseDown;
            MouseMove += Box_MouseMove;
            MouseUp += Box_MouseUp;
            contextMenu.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private ContextMenuStrip contextMenu;
        private ToolStripMenuItem openMenu;
        private ToolStripMenuItem resizeMenu;
    }
}

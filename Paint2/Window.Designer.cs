﻿namespace Paint2
{
    partial class Window
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            toolPanel = new Panel();
            colorPanel = new Panel();
            comboBox = new ComboBox();
            mainMenu = new MenuStrip();
            fileMenu = new ToolStripMenuItem();
            openMenu = new ToolStripMenuItem();
            saveMenu = new ToolStripMenuItem();
            toolStripSeparator1 = new ToolStripSeparator();
            quitMenu = new ToolStripMenuItem();
            splitContainer1 = new SplitContainer();
            splitContainer2 = new SplitContainer();
            tree = new TreeView();
            splitContainer3 = new SplitContainer();
            pictureBox = new PictureBox();
            prop = new PropertyGrid();
            info = new TextBox();
            toolPanel.SuspendLayout();
            mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer2).BeginInit();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer3).BeginInit();
            splitContainer3.Panel1.SuspendLayout();
            splitContainer3.Panel2.SuspendLayout();
            splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox).BeginInit();
            SuspendLayout();
            // 
            // toolPanel
            // 
            toolPanel.BackColor = Color.FromArgb(255, 255, 192);
            toolPanel.BorderStyle = BorderStyle.Fixed3D;
            toolPanel.Controls.Add(colorPanel);
            toolPanel.Controls.Add(comboBox);
            toolPanel.Dock = DockStyle.Top;
            toolPanel.Location = new Point(0, 24);
            toolPanel.Name = "toolPanel";
            toolPanel.Size = new Size(800, 46);
            toolPanel.TabIndex = 0;
            // 
            // colorPanel
            // 
            colorPanel.BackColor = Color.Blue;
            colorPanel.Location = new Point(7, 6);
            colorPanel.Name = "colorPanel";
            colorPanel.Size = new Size(63, 27);
            colorPanel.TabIndex = 1;
            colorPanel.MouseDown += colorPanel_MouseDown;
            // 
            // comboBox
            // 
            comboBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.FormattingEnabled = true;
            comboBox.Items.AddRange(new object[] { "line", "rectangle", "ellipse" });
            comboBox.Location = new Point(665, 10);
            comboBox.Name = "comboBox";
            comboBox.Size = new Size(121, 23);
            comboBox.TabIndex = 0;
            // 
            // mainMenu
            // 
            mainMenu.Items.AddRange(new ToolStripItem[] { fileMenu });
            mainMenu.Location = new Point(0, 0);
            mainMenu.Name = "mainMenu";
            mainMenu.Size = new Size(800, 24);
            mainMenu.TabIndex = 2;
            mainMenu.Text = "mainMenu";
            // 
            // fileMenu
            // 
            fileMenu.DropDownItems.AddRange(new ToolStripItem[] { openMenu, saveMenu, toolStripSeparator1, quitMenu });
            fileMenu.Name = "fileMenu";
            fileMenu.Size = new Size(37, 20);
            fileMenu.Text = "&File";
            // 
            // openMenu
            // 
            openMenu.Name = "openMenu";
            openMenu.ShortcutKeys = Keys.Control | Keys.O;
            openMenu.Size = new Size(146, 22);
            openMenu.Text = "&Open";
            openMenu.Click += openMenu_Click;
            // 
            // saveMenu
            // 
            saveMenu.Name = "saveMenu";
            saveMenu.ShortcutKeys = Keys.Control | Keys.S;
            saveMenu.Size = new Size(146, 22);
            saveMenu.Text = "&Save";
            saveMenu.Click += saveMenu_Click;
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new Size(143, 6);
            // 
            // quitMenu
            // 
            quitMenu.Name = "quitMenu";
            quitMenu.ShortcutKeys = Keys.Control | Keys.Q;
            quitMenu.Size = new Size(146, 22);
            quitMenu.Text = "&Quit";
            quitMenu.Click += quitMenu_Click;
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(0, 70);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(info);
            splitContainer1.Size = new Size(800, 380);
            splitContainer1.SplitterDistance = 328;
            splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            splitContainer2.Dock = DockStyle.Fill;
            splitContainer2.Location = new Point(0, 0);
            splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            splitContainer2.Panel1.Controls.Add(tree);
            // 
            // splitContainer2.Panel2
            // 
            splitContainer2.Panel2.Controls.Add(splitContainer3);
            splitContainer2.Size = new Size(800, 328);
            splitContainer2.SplitterDistance = 266;
            splitContainer2.TabIndex = 0;
            // 
            // tree
            // 
            tree.Dock = DockStyle.Fill;
            tree.Location = new Point(0, 0);
            tree.Name = "tree";
            tree.Size = new Size(266, 328);
            tree.TabIndex = 0;
            // 
            // splitContainer3
            // 
            splitContainer3.Dock = DockStyle.Fill;
            splitContainer3.Location = new Point(0, 0);
            splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            splitContainer3.Panel1.Controls.Add(pictureBox);
            splitContainer3.Panel1MinSize = 400;
            // 
            // splitContainer3.Panel2
            // 
            splitContainer3.Panel2.Controls.Add(prop);
            splitContainer3.Size = new Size(530, 328);
            splitContainer3.SplitterDistance = 400;
            splitContainer3.TabIndex = 0;
            // 
            // pictureBox
            // 
            pictureBox.BackColor = SystemColors.Info;
            pictureBox.Dock = DockStyle.Fill;
            pictureBox.Location = new Point(0, 0);
            pictureBox.Name = "pictureBox";
            pictureBox.Size = new Size(400, 328);
            pictureBox.TabIndex = 2;
            pictureBox.TabStop = false;
            pictureBox.SizeChanged += pictureBox_SizeChanged;
            // 
            // prop
            // 
            prop.Dock = DockStyle.Fill;
            prop.Location = new Point(0, 0);
            prop.Name = "prop";
            prop.SelectedObject = toolPanel;
            prop.Size = new Size(126, 328);
            prop.TabIndex = 0;
            // 
            // info
            // 
            info.Dock = DockStyle.Fill;
            info.Location = new Point(0, 0);
            info.Multiline = true;
            info.Name = "info";
            info.Size = new Size(800, 48);
            info.TabIndex = 0;
            // 
            // Window
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(splitContainer1);
            Controls.Add(toolPanel);
            Controls.Add(mainMenu);
            MainMenuStrip = mainMenu;
            Name = "Window";
            Text = "Paint";
            toolPanel.ResumeLayout(false);
            mainMenu.ResumeLayout(false);
            mainMenu.PerformLayout();
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer2).EndInit();
            splitContainer2.ResumeLayout(false);
            splitContainer3.Panel1.ResumeLayout(false);
            splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer3).EndInit();
            splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Panel toolPanel;
        private ComboBox comboBox;
        private Panel colorPanel;
        private MenuStrip mainMenu;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem openMenu;
        private ToolStripMenuItem saveMenu;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem quitMenu;
        private SplitContainer splitContainer1;
        private SplitContainer splitContainer2;
        private TreeView tree;
        private SplitContainer splitContainer3;
        private PictureBox pictureBox;
        private PropertyGrid prop;
        private TextBox info;
    }
}

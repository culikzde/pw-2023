﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paint2
{
    public partial class Box : UserControl
    {
        public Box()
        {
            InitializeComponent();
        }


        private int X0, Y0;
        private bool click = false;

        private void Box_MouseDown(object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void Box_MouseMove(object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Middle || Control.ModifierKeys == Keys.Shift)
                {
                    ColorDialog dlg = new ColorDialog();
                    dlg.Color = BackColor;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        BackColor = dlg.Color;
                    }
                }
                else if (e.Button == MouseButtons.Right || Control.ModifierKeys == Keys.Control)
                {
                    Width += e.X - X0;
                    Height += e.Y - Y0;

                    X0 = e.X;
                    Y0 = e.Y;
                }
                else if (e.Button == MouseButtons.Left)
                {
                    Left += e.X - X0;
                    this.Top = this.Top + e.Y - Y0;
                }
            }
        }

        private void Box_MouseUp(object sender, MouseEventArgs e)
        {
            click = false;
        }

        private void openMenu_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                BackgroundImage = new Bitmap(dlg.FileName);
            }
        }

        private void resizeMenu_Click(object sender, EventArgs e)
        {
            resizeMenu.Checked = ! resizeMenu.Checked;
            if (resizeMenu.Checked)
                BackgroundImageLayout = ImageLayout.Stretch;
            else
                BackgroundImageLayout = ImageLayout.None;
        }
    }
}

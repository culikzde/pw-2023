﻿namespace Draw2
{
    partial class Window
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            toolPanel = new Panel();
            colorPanel = new Panel();
            comboBox = new ComboBox();
            pictureBox = new PictureBox();
            mainMenu = new MenuStrip();
            fileMenu = new ToolStripMenuItem();
            saveMenu = new ToolStripMenuItem();
            openMenu = new ToolStripMenuItem();
            menuSeparator = new ToolStripSeparator();
            quitMenu = new ToolStripMenuItem();
            statusBar = new StatusStrip();
            splitter1 = new SplitContainer();
            splitter2 = new SplitContainer();
            tree = new TreeView();
            splitter3 = new SplitContainer();
            prop = new PropertyGrid();
            toolPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox).BeginInit();
            mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitter1).BeginInit();
            splitter1.Panel1.SuspendLayout();
            splitter1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitter2).BeginInit();
            splitter2.Panel1.SuspendLayout();
            splitter2.Panel2.SuspendLayout();
            splitter2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitter3).BeginInit();
            splitter3.Panel1.SuspendLayout();
            splitter3.Panel2.SuspendLayout();
            splitter3.SuspendLayout();
            SuspendLayout();
            // 
            // toolPanel
            // 
            toolPanel.BackColor = Color.Silver;
            toolPanel.Controls.Add(colorPanel);
            toolPanel.Controls.Add(comboBox);
            toolPanel.Dock = DockStyle.Top;
            toolPanel.ForeColor = Color.Black;
            toolPanel.Location = new Point(0, 24);
            toolPanel.Name = "toolPanel";
            toolPanel.Size = new Size(800, 44);
            toolPanel.TabIndex = 0;
            // 
            // colorPanel
            // 
            colorPanel.BackColor = Color.Blue;
            colorPanel.Location = new Point(6, 5);
            colorPanel.Name = "colorPanel";
            colorPanel.Size = new Size(73, 33);
            colorPanel.TabIndex = 1;
            colorPanel.MouseDown += colorPanel_MouseDown;
            // 
            // comboBox
            // 
            comboBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.FormattingEnabled = true;
            comboBox.Items.AddRange(new object[] { "line", "rectangle", "ellipse" });
            comboBox.Location = new Point(653, 12);
            comboBox.Name = "comboBox";
            comboBox.Size = new Size(121, 23);
            comboBox.TabIndex = 0;
            // 
            // pictureBox
            // 
            pictureBox.BackColor = Color.FromArgb(255, 255, 192);
            pictureBox.Dock = DockStyle.Fill;
            pictureBox.Location = new Point(0, 0);
            pictureBox.Name = "pictureBox";
            pictureBox.Size = new Size(258, 266);
            pictureBox.TabIndex = 1;
            pictureBox.TabStop = false;
            pictureBox.SizeChanged += pictureBox_SizeChanged;
            pictureBox.MouseDown += pictureBox_MouseDown;
            pictureBox.MouseMove += pictureBox_MouseMove;
            pictureBox.MouseUp += pictureBox_MouseUp;
            // 
            // mainMenu
            // 
            mainMenu.Items.AddRange(new ToolStripItem[] { fileMenu });
            mainMenu.Location = new Point(0, 0);
            mainMenu.Name = "mainMenu";
            mainMenu.Size = new Size(800, 24);
            mainMenu.TabIndex = 2;
            mainMenu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            fileMenu.DropDownItems.AddRange(new ToolStripItem[] { saveMenu, openMenu, menuSeparator, quitMenu });
            fileMenu.Name = "fileMenu";
            fileMenu.Size = new Size(37, 20);
            fileMenu.Text = "&File";
            // 
            // saveMenu
            // 
            saveMenu.Name = "saveMenu";
            saveMenu.ShortcutKeys = Keys.Control | Keys.S;
            saveMenu.Size = new Size(146, 22);
            saveMenu.Text = "&Save";
            saveMenu.Click += saveMenu_Click;
            // 
            // openMenu
            // 
            openMenu.Name = "openMenu";
            openMenu.ShortcutKeys = Keys.Control | Keys.O;
            openMenu.Size = new Size(146, 22);
            openMenu.Text = "&Open";
            openMenu.Click += openMenu_Click;
            // 
            // menuSeparator
            // 
            menuSeparator.Name = "menuSeparator";
            menuSeparator.Size = new Size(143, 6);
            // 
            // quitMenu
            // 
            quitMenu.Name = "quitMenu";
            quitMenu.ShortcutKeys = Keys.Control | Keys.Q;
            quitMenu.Size = new Size(146, 22);
            quitMenu.Text = "&Quit";
            quitMenu.Click += quitMenu_Click;
            // 
            // statusBar
            // 
            statusBar.Location = new Point(0, 428);
            statusBar.Name = "statusBar";
            statusBar.Size = new Size(800, 22);
            statusBar.TabIndex = 3;
            statusBar.Text = "statusStrip1";
            // 
            // splitter1
            // 
            splitter1.Dock = DockStyle.Fill;
            splitter1.Location = new Point(0, 68);
            splitter1.Name = "splitter1";
            splitter1.Orientation = Orientation.Horizontal;
            // 
            // splitter1.Panel1
            // 
            splitter1.Panel1.Controls.Add(splitter2);
            splitter1.Size = new Size(800, 360);
            splitter1.SplitterDistance = 266;
            splitter1.TabIndex = 4;
            // 
            // splitter2
            // 
            splitter2.Dock = DockStyle.Fill;
            splitter2.Location = new Point(0, 0);
            splitter2.Name = "splitter2";
            // 
            // splitter2.Panel1
            // 
            splitter2.Panel1.Controls.Add(tree);
            // 
            // splitter2.Panel2
            // 
            splitter2.Panel2.Controls.Add(splitter3);
            splitter2.Size = new Size(800, 266);
            splitter2.SplitterDistance = 266;
            splitter2.TabIndex = 0;
            // 
            // tree
            // 
            tree.Dock = DockStyle.Fill;
            tree.Location = new Point(0, 0);
            tree.Name = "tree";
            tree.Size = new Size(266, 266);
            tree.TabIndex = 0;
            tree.AfterSelect += tree_AfterSelect;
            // 
            // splitter3
            // 
            splitter3.Dock = DockStyle.Fill;
            splitter3.Location = new Point(0, 0);
            splitter3.Name = "splitter3";
            // 
            // splitter3.Panel1
            // 
            splitter3.Panel1.Controls.Add(pictureBox);
            // 
            // splitter3.Panel2
            // 
            splitter3.Panel2.Controls.Add(prop);
            splitter3.Size = new Size(530, 266);
            splitter3.SplitterDistance = 258;
            splitter3.TabIndex = 0;
            // 
            // prop
            // 
            prop.Dock = DockStyle.Fill;
            prop.Location = new Point(0, 0);
            prop.Name = "prop";
            prop.Size = new Size(268, 266);
            prop.TabIndex = 0;
            // 
            // Window
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(splitter1);
            Controls.Add(statusBar);
            Controls.Add(toolPanel);
            Controls.Add(mainMenu);
            MainMenuStrip = mainMenu;
            Name = "Window";
            Text = "Form1";
            toolPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox).EndInit();
            mainMenu.ResumeLayout(false);
            mainMenu.PerformLayout();
            splitter1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitter1).EndInit();
            splitter1.ResumeLayout(false);
            splitter2.Panel1.ResumeLayout(false);
            splitter2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitter2).EndInit();
            splitter2.ResumeLayout(false);
            splitter3.Panel1.ResumeLayout(false);
            splitter3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitter3).EndInit();
            splitter3.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Panel toolPanel;
        private PictureBox pictureBox;
        private ComboBox comboBox;
        private Panel colorPanel;
        private MenuStrip mainMenu;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem saveMenu;
        private ToolStripMenuItem openMenu;
        private ToolStripSeparator menuSeparator;
        private ToolStripMenuItem quitMenu;
        private StatusStrip statusBar;
        private SplitContainer splitter1;
        private SplitContainer splitter2;
        private SplitContainer splitter3;
        private TreeView tree;
        private PropertyGrid prop;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Draw2
{
    public class BoxData
    {
        public String name { get; set; }

        public int X { get => x; set => x = value; }

        public int Y { 
                       get { return y; }
                       set { y = value; } 
                     }
        public int Z { get; set; }

        private int y;
        private int x;
    }
}

using System.Drawing.Imaging;
using System.Xml.Linq;

namespace Draw2
{
    public partial class Window : Form
    {
        const int lineTool = 0;
        const int rectangleTool = 1;
        const int ellipseTool = 2;

        public Window()
        {
            InitializeComponent();
            comboBox.SelectedIndex = lineTool;
            // pen.Color = colorPanel.BackColor;
            addButton(Color.Red);
            addButton(Color.Green);
            addButton(Color.Yellow);
            pictureBox_SizeChanged(null, null);

            Box box = new Box();
            box.Parent = pictureBox;

            Box box2 = new Box();
            box2.Parent = pictureBox;
            box2.BackColor = Color.Lime;

            dirBranch ("../../../..");
            objectTree ();
            blockTree ();

            if (false)
            {
                Graphics g = Graphics.FromImage(pictureBox.Image);
                for (int x = 10; x <= 70; x++)
                {
                    int y = 10;
                    int s = 1;
                    Brush b = new SolidBrush(Color.FromArgb(0, 0, 255));
                    g.FillRectangle(b, x, y, s, s);
                }
            }

            if (false)
            {
                int w = pictureBox.Width;
                int h = pictureBox.Height;
                Bitmap bmp = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                byte[] data = new byte[4 * w * h];
                {
                    int y = 20;
                    for (int x = 10; x <= 80; x++)
                    {
                        int i = 4 * (x + y * w);
                        data[i + 1] = 255;
                        data[i + 3] = 255;
                    }
                }

                for (int y = 0; y < h; y++)
                    for (int x = 0; x < w; x++)
                    {
                        int i = 4 * (x + y * w);
                        data[i] = (byte)(x % 256);
                        data[i + 1] = (byte)(y & 0xff);
                        data[i + 2] = 0;
                        data[i + 3] = 255;
                    }

                Rectangle dimension = new Rectangle(0, 0, bmp.Width, bmp.Height);
                BitmapData picData = bmp.LockBits(dimension, ImageLockMode.ReadWrite, bmp.PixelFormat);
                IntPtr pixelStartAddress = picData.Scan0;
                System.Runtime.InteropServices.Marshal.Copy(data, 0, pixelStartAddress, data.Length);
                bmp.UnlockBits(picData);

                pictureBox.Image = bmp;
            }
        }
        private void blockTree()
        {
            TreeNode root = new TreeNode();
            root.Text = "Blocks";
            root.ForeColor = Color.Red;
            tree.Nodes.Add(root);

            foreach (Control item in pictureBox.Controls)
                blockBranch(root.Nodes, item);

            root.ExpandAll();
        }

        private void blockBranch(TreeNodeCollection target, Object obj)
        {
            if (obj is Box)
            {
                Box box = obj as Box;                
                
                TreeNode node = new TreeNode();
                node.Text = box.Name + " : " + box.GetType();
                node.ForeColor = box.BackColor;
                node.Tag = box.data;
                target.Add(node);

                foreach (Control item in box.Controls)
                    blockBranch(node.Nodes, item);
            }
        }

        private void objectTree()
        {
            TreeNode root = new TreeNode();
            root.Text = "Controls";
            root.ForeColor = Color.Red;
            tree.Nodes.Add(root);

            objectBranch(root.Nodes, this);
        }
                

        private void objectBranch (TreeNodeCollection target, Control obj)
        {
            TreeNode node = new TreeNode();
            node.Text = obj.Name + " : " + obj.GetType();
            node.ForeColor = Color.Green;
            node.Tag = obj;
            target.Add (node);

            foreach (Control item in obj.Controls)
                objectBranch (node.Nodes, item);
        }

        private void dirTree(TreeNodeCollection target, String path, int level = 1)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            foreach (DirectoryInfo d in dir.GetDirectories())
            {
                TreeNode node = new TreeNode();
                node.Text = d.Name;
                node.ForeColor = Color.Red;
                target.Add(node);
                if (level <= 3)
                    dirTree(node.Nodes, d.FullName, level + 1);
            }
            foreach (FileInfo f in dir.GetFiles())
            {
                TreeNode node = new TreeNode();
                node.Text = f.Name;
                node.ForeColor = Color.Blue;
                target.Add(node);
            }
        }

        private void dirBranch(String path)
        {
            TreeNode root = new TreeNode();
            root.Text = "Files";
            root.ForeColor = Color.Red;
            tree.Nodes.Add(root);

            dirTree(root.Nodes, path);

            // root.ExpandAll();
        }

        private void tree_AfterSelect (object sender, TreeViewEventArgs e)
        {
            object obj = e.Node.Tag;
            prop.SelectedObject = obj;
        }

        private void pictureBox_SizeChanged(object sender, EventArgs e)
        {
            Image old = pictureBox.Image;

            int w = pictureBox.Width;
            int h = pictureBox.Height;

            if (old != null)
            {
                if (old.Width > w) w = old.Width;
                if (old.Height > h) h = old.Height;
            }

            Bitmap bmp = new Bitmap(w, h);

            Graphics g = Graphics.FromImage(bmp);
            Brush b = new SolidBrush(Color.White);
            g.FillRectangle(b, 0, 0, w, h);

            if (old != null)
            {
                g.DrawImage(old, 0, 0);
            }

            pictureBox.Image = bmp;
        }

        private int X0, Y0;
        private bool press = false;
        private Bitmap save;

        private Pen pen = new Pen(Color.Red, 3);
        private SolidBrush brush = new SolidBrush(Color.Yellow);

        private void colorPanel_MouseDown(object sender, MouseEventArgs e)
        {
            // Panel p = (Panel) sender;
            Panel p = sender as Panel;
            // if (sender is Panel) { }

            if (e.Button == MouseButtons.Middle || Control.ModifierKeys == Keys.Shift)
            {
                ColorDialog dlg = new ColorDialog();
                dlg.Color = p.BackColor;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    p.BackColor = dlg.Color;
                    pen.Color = p.BackColor;
                }
            }
            else if (e.Button == MouseButtons.Right || Control.ModifierKeys == Keys.Control)
                brush.Color = p.BackColor;
            else if (e.Button == MouseButtons.Left)
                pen.Color = p.BackColor;
        }

        int cnt = 1;

        private void addButton(Color color)
        {
            Panel p = new Panel();
            p.BackColor = color;
            p.Parent = toolPanel;
            // toolPanel.Controls.Add(p);

            p.Width = colorPanel.Width;
            p.Height = colorPanel.Height;
            p.Top = colorPanel.Top;
            p.Left = (cnt + 1) * colorPanel.Left + cnt * colorPanel.Width;
            cnt++;

            p.MouseDown += colorPanel_MouseDown;
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            press = true;
            save = new Bitmap(pictureBox.Image);
        }
        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (press)
            {
                int tool = comboBox.SelectedIndex;

                Graphics g = Graphics.FromImage(pictureBox.Image);
                // Pen p = new Pen(Color.FromArgb(255, 0, 0), 3);
                g.DrawImage(save, 0, 0);

                switch (tool)
                {
                    case lineTool:
                        g.DrawLine(pen, X0, Y0, e.X, e.Y);
                        break;

                    case rectangleTool:
                        int X1 = X0;
                        int Y1 = Y0;
                        int X2 = e.X;
                        int Y2 = e.Y;
                        if (X1 > X2) { int t = X1; X1 = X2; X2 = t; }
                        if (Y1 > Y2) { int t = Y1; Y1 = Y2; Y2 = t; }
                        g.FillRectangle(brush, X1, Y1, X2 - X1, Y2 - Y1);
                        g.DrawRectangle(pen, X1, Y1, X2 - X1, Y2 - Y1);
                        break;

                    case ellipseTool:
                        g.FillEllipse(brush, X0, Y0, e.X - X0, e.Y - Y0);
                        g.DrawEllipse(pen, X0, Y0, e.X - X0, e.Y - Y0);
                        break;
                }
                pictureBox.Invalidate();
            }

        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            press = false;
        }

        private void openMenu_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = dlg.FileName;
                Bitmap bitmap = new Bitmap(fileName);
                pictureBox.Image = bitmap;
                pictureBox_SizeChanged(null, null); // resize image
            }

        }

        private void saveMenu_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = dlg.FileName;
                pictureBox.Image.Save(fileName, ImageFormat.Bmp);
            }
        }

        private void quitMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Draw2
{
    public partial class Box : UserControl
    {
        public BoxData data;

        public Box()
        {
            InitializeComponent();

            data = new BoxData();
            data.name = Name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = "Click";

            Bitmap b = new Bitmap("c:\\temp\\tulip.bmp");
            // ImageAlg.ApplyEmboss (ref b);
            ImageAlg.ApplyGridPixelate (ref b, new Size (16, 16));
            // ImageAlg.ApplyGrayscale(ref b);
            // ImageAlg.ApplySharpen (ref b, 100);
            BackgroundImage = b;

            BackgroundImageLayout = ImageLayout.Stretch;
        }

        private int X0, Y0;
        private bool click = false;

        private void Box_MouseDown(object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void Box_MouseMove(object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Right || Control.ModifierKeys == Keys.Control)
                {
                    // resize
                    this.Width += e.X - X0;
                    Height += e.Y - Y0;

                    X0 = e.X;
                    Y0 = e.Y;
                }
                else
                {
                    // move
                    this.Left += e.X - X0;
                    Top += e.Y - Y0;
                }
            }
        }

        private void Box_MouseUp(object sender, MouseEventArgs e)
        {
            click = false;
        }
    }
}

#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->treeWidget->header()->hide();


    QTreeWidgetItem * root = new QTreeWidgetItem;
    root->setText (0, "koren stromu");

    for (int i = 1; i <= 3; i=i+1)
    {
        QTreeWidgetItem * branch = new QTreeWidgetItem;
        branch->setText (0, "vetev " + QString::number (i));
        root->addChild (branch);
    }


    ui->treeWidget->addTopLevelItem (root);
    root->setExpanded (true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

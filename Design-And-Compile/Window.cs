using System.Drawing.Imaging;

// using System.IO;
using System.Reflection;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Windows.Forms;

namespace Designer
{
    public partial class Window : Form
    {

        private const int lineTool = 0;
        private const int rectangleTool = 1;
        private const int ellipseTool = 2;

        private Pen pen = new Pen(Color.FromArgb(255, 0, 0), 3);
        private SolidBrush brush = new SolidBrush(Color.Yellow);

        private int colorCnt = 1;

        public Window()
        {
            InitializeComponent();
            pictureBox_SizeChanged(null, null);

            comboBox.SelectedIndex = lineTool;
            // palette.SelectedTab = originalPage;

            addButton(Color.Red);
            addButton(Color.Green);
            addButton(Color.Yellow);

            Box box = new Box();
            box.Parent = pictureBox;
            box.Left = 20;
            box.Top = 20;
            box.info = info;

            Box box2 = new Box();
            box2.Parent = pictureBox;
            box2.BackColor = Color.DarkOrange;
            box2.Left = 40;
            box2.Top = 40;
            box2.info = info;

            pictureBox.AllowDrop = true;

            createColorPanels(new List<String> { "red", "green", "blue", "yellow", "orange", "lime", "cornflowerblue" });

            createComponent(typeof(Box));
            createComponent(typeof(Button));
            createComponent(typeof(Panel));
            createComponent(typeof(TreeView));
            createComponent(typeof(ListView));
            createComponent(typeof(TextBox));
            createComponent(typeof(CheckBox));


            edit.Text =
            @"
            namespace MyNamespace
            {
                public class MyClass
                {
                    public string MyMethod ()
                    {
                        return ""Hello, World!"";
                    }
                }
            }
            ";

            createDesignSurface();
        }
        private void createComponent(Type type0)
        {
            Factory f = new Factory(type0);
            componentToolbar.Items.Add(f);
        }

        private void createColorPanels(List<String> colorNames)
        {
            foreach (var colorName in colorNames)
            {
                Color color = Color.FromName(colorName);
                ColorButton p = new ColorButton(color, colorName);
                colorToolbar.Items.Add(p);
            }
        }

        private void pictureBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(Color)))
                e.Effect = DragDropEffects.Move;

            // else if (e.Data.GetDataPresent (DataFormats.Text))
            //    e.Effect = DragDropEffects.Copy;
            // else if (e.Data.GetDataPresent ("System.Windows.Forms.Button"))
            //     e.Effect = DragDropEffects.Copy;
            /*
            else if (e.Data.GetDataPresent("Builder.Factory"))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
            */
        }

        private void pictureBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(Color)))
                brush.Color = (Color)e.Data.GetData(typeof(Color));


            /*
            if (e.Data.GetDataPresent("Builder.Factory"))
            {
                Factory factory = e.Data.GetData("Builder.Factory") as Factory;
                Point location = designer.PointToClient(new Point(e.X, e.Y));
                createObject(factory, null, location);
            }
            */
        }

        private void addButton(Color color)
        {
            Panel panel = new Panel();
            panel.Width = colorPanel.Width;
            panel.Height = colorPanel.Height;
            panel.Top = colorPanel.Top; // Y
            panel.Left = colorCnt * colorPanel.Width + (colorCnt + 1) * colorPanel.Left; // X
            colorCnt++;
            // panel.Parent = toolPanel;
            toolPanel.Controls.Add(panel);
            panel.BackColor = color;
            panel.MouseDown += colorPanel_MouseDown;
        }

        private void colorPanel_MouseDown(object sender, MouseEventArgs e)
        {
            // Panel panel = (Panel) sender ;
            // if (sender is Panel) { }
            Panel panel = sender as Panel;
            if (e.Button == MouseButtons.Middle || Control.ModifierKeys == Keys.Shift)
            {
                ColorDialog dlg = new ColorDialog();
                dlg.Color = panel.BackColor;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    panel.BackColor = dlg.Color;
                    pen.Color = panel.BackColor;
                }
            }
            else if (e.Button == MouseButtons.Right || Control.ModifierKeys == Keys.Control)
                brush.Color = panel.BackColor;
            else if (e.Button == MouseButtons.Left)
                pen.Color = panel.BackColor;
        }

        private void pictureBox_SizeChanged(object sender, EventArgs e)
        {
            Image old = pictureBox.Image;

            int w = pictureBox.Width;
            int h = pictureBox.Height;
            if (old != null)
            {
                if (w < old.Width) w = old.Width;
                if (h < old.Height) h = old.Height;
            }

            Bitmap image = new Bitmap(w, h);
            Graphics g = Graphics.FromImage(image);
            Brush b = new SolidBrush(Color.White);
            g.FillRectangle(b, 0, 0, w, h);

            if (old != null)
            {
                g.DrawImage(old, 0, 0);
            }

            pictureBox.Image = image;

            if (false)
            {
                w = pictureBox.Width;
                h = pictureBox.Height;
                Bitmap bmp = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

                byte[] data = new byte[4 * w * h];
                {
                    int y = 20;
                    for (int x = 10; x <= 80; x++)
                    {
                        int i = 4 * (x + y * w);
                        data[i + 1] = 255; // Green
                        data[i + 3] = 255; // Alpha
                    }
                }

                for (int y = 0; y < h; y++)
                    for (int x = 0; x < w; x++)
                    {
                        int i = 4 * (x + y * w);
                        data[i] = (byte)(x % 256); // Blue
                        data[i + 1] = (byte)(y & 0xff); // Green
                        data[i + 2] = 0; // Red
                        data[i + 3] = 255; // Alpha
                    }

                // http://stackoverflow.com/questions/68717508/receiving-double-image-with-bitmap-on-c-sharp
                Rectangle dimension = new Rectangle(0, 0, bmp.Width, bmp.Height);
                BitmapData picData = bmp.LockBits(dimension, ImageLockMode.ReadWrite, bmp.PixelFormat);
                IntPtr pixelStartAddress = picData.Scan0;
                System.Runtime.InteropServices.Marshal.Copy(data, 0, pixelStartAddress, data.Length);
                bmp.UnlockBits(picData);

                pictureBox.Image = bmp;
            }

        }

        private int X0, Y0;
        private bool click = false;
        private Bitmap save;

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            click = true;
            save = new Bitmap(pictureBox.Image);
        }
        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (click)
            {
                int tool = comboBox.SelectedIndex;

                Graphics g = Graphics.FromImage(pictureBox.Image);
                g.DrawImage(save, 0, 0);
                switch (tool)
                {
                    case lineTool:
                        g.DrawLine(pen, X0, Y0, e.X, e.Y);
                        break;
                    case rectangleTool:
                        int X1 = X0;
                        int Y1 = Y0;
                        int X2 = e.X;
                        int Y2 = e.Y;
                        if (X1 > X2) { int t = X1; X1 = X2; X2 = t; }
                        if (Y1 > Y2) { int t = Y1; Y1 = Y2; Y2 = t; }
                        g.FillRectangle(brush, X1, Y1, X2 - X1, Y2 - Y1);
                        g.DrawRectangle(pen, X1, Y1, X2 - X1, Y2 - Y1);
                        break;
                    case ellipseTool:
                        g.FillEllipse(brush, X0, Y0, e.X - X0, e.Y - Y0);
                        g.DrawEllipse(pen, X0, Y0, e.X - X0, e.Y - Y0);
                        break;
                }
                pictureBox.Invalidate();
            }
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            click = false;
        }

        private void openMenu_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = dlg.FileName;
                Bitmap bitmap = new Bitmap(fileName);
                pictureBox.Image = bitmap;
                pictureBox_SizeChanged(null, null); // resize image
            }
        }

        private void saveMenu_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = dlg.FileName;
                pictureBox.Image.Save(fileName, ImageFormat.Bmp);
            }

        }
        private void displayTree(TreeNodeCollection nodes, Control obj)
        {
            var node = new TreeNode();
            node.Text = obj.Name + " : " + obj.GetType().Name;
            node.Tag = obj;
            nodes.Add(node);

            foreach (var item in obj.Controls)
                if (item is Control)
                    displayTree(node.Nodes, item as Control);

        }

        private void treeMenu_Click(object sender, EventArgs e)
        {
            tree.Nodes.Clear();
            foreach (var item in pictureBox.Controls)
                if (item is Control)
                    displayTree(tree.Nodes, item as Control);
            tree.ExpandAll();
        }

        private void tree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            prop.SelectedObject = e.Node.Tag;

            object obj = e.Node.Tag;
            ObjectWrapper wrap = new ObjectWrapper(obj);
            prop.SelectedObject = wrap;
        }

        private void print(string msg)
        {
            info.AppendText(msg + "\r\n");
        }

        private void displaySyntax(TreeNodeCollection target, SyntaxNode node)
        {
            TreeNode branch = new TreeNode();
            branch.Text = node.ToString();
            branch.Tag = node;

            target.Add(branch);

            foreach (SyntaxNode item in node.ChildNodes())
                displaySyntax(branch.Nodes, item);
        }

        private void runMenu_Click(object sender, EventArgs e)
        {
            info.Clear();
            tabs.SelectedTab = editorPage;

            string source = edit.Text;
            var syntaxTree = CSharpSyntaxTree.ParseText(source);

            string code = syntaxTree.ToString();
            // print(code);
            displaySyntax(tree.Nodes, syntaxTree.GetRoot());

            CSharpCompilation compilation = CSharpCompilation.Create(
                "assemblyName",
                new[] { syntaxTree },
                new[] { MetadataReference.CreateFromFile(typeof(object).Assembly.Location) },
                new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var dllStream = new MemoryStream())
            using (var pdbStream = new MemoryStream())
            {
                var emitResult = compilation.Emit(dllStream, pdbStream);
                if (!emitResult.Success)
                {
                    print("ERROR");
                    foreach (var diagnostic in emitResult.Diagnostics)
                        print(diagnostic.ToString());
                }
                else
                {
                    print("O.K.");
                    dllStream.Seek(0, SeekOrigin.Begin);

                    // Load the compiled assembly
                    var assembly = Assembly.Load(dllStream.ToArray());

                    // Execute the library code
                    var libraryClassType = assembly.GetType("MyNamespace.MyClass");
                    var libraryInstance = Activator.CreateInstance(libraryClassType);
                    var libraryMethod = libraryClassType.GetMethod("MyMethod");
                    var result = libraryMethod.Invoke(libraryInstance, null);
                    print("RESULT: " + result);
                }

                foreach (var diagnostic in emitResult.Diagnostics)
                {
                    Console.WriteLine(diagnostic.ToString());
                }
            }
        }

        private void createDesignSurface ()
        {
            // http://www.cnblogs.com/Files/panjiwen/FormDesigner.zip
            // https://www.vbforums.com/showthread.php?891758-NET-visual-form-designer-only-50-lines-of-code%26%2365281%3B%26%2365281%3B%26%2365281%3B

            DesignSurface surface = new DesignSurface();
            surface.BeginLoad(typeof(Form));
            Control view = surface.View as Control;
            view.Dock = DockStyle.Fill;
            designSurfacePage.Controls.Add(view);

            // prop.SelectedObject = surface.ComponentContainer.Components[0];
            // https://github.com/PavelTorgashov/FastColoredTextBox
        }

        private void quitMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}

// NuGet package Microsoft.CodeAnalysis.CSharp
// Solution Explorer, Compiler Project, References, Manage NuGet packages
// Menu Project, Manage NuGet packages

// using System.IO;
// using Microsoft.CodeAnalysis;
// using Microsoft.CodeAnalysis.CSharp;

// https://stackoverflow.com/questions/32769630/how-to-compile-a-c-sharp-file-with-roslyn-programmatically
// https://stackoverflow.com/questions/4181668/execute-c-sharp-code-at-runtime-from-code-file
// https://www.linkedin.com/pulse/load-compile-run-c-code-dynamically-munib-butt


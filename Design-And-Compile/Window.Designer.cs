﻿namespace Designer
{
    partial class Window
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            mainMenu = new MenuStrip();
            fileMenu = new ToolStripMenuItem();
            openMenu = new ToolStripMenuItem();
            saveMenu = new ToolStripMenuItem();
            menuSeparator1 = new ToolStripSeparator();
            quitMenu = new ToolStripMenuItem();
            editMenu = new ToolStripMenuItem();
            viewMenu = new ToolStripMenuItem();
            treeMenu = new ToolStripMenuItem();
            codeMenu = new ToolStripMenuItem();
            runMenu = new ToolStripMenuItem();
            splitContainer1 = new SplitContainer();
            splitContainer2 = new SplitContainer();
            tree = new TreeView();
            splitContainer3 = new SplitContainer();
            tabs = new TabControl();
            designerPage = new TabPage();
            pictureBox = new PictureBox();
            editorPage = new TabPage();
            edit = new TextBox();
            designSurfacePage = new TabPage();
            prop = new PropertyGrid();
            info = new TextBox();
            palette = new TabControl();
            colorPage = new TabPage();
            colorToolbar = new ToolStrip();
            componentPage = new TabPage();
            componentToolbar = new ToolStrip();
            originalPage = new TabPage();
            toolPanel = new Panel();
            comboBox = new ComboBox();
            colorPanel = new Panel();
            mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer2).BeginInit();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer3).BeginInit();
            splitContainer3.Panel1.SuspendLayout();
            splitContainer3.Panel2.SuspendLayout();
            splitContainer3.SuspendLayout();
            tabs.SuspendLayout();
            designerPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox).BeginInit();
            editorPage.SuspendLayout();
            palette.SuspendLayout();
            colorPage.SuspendLayout();
            componentPage.SuspendLayout();
            originalPage.SuspendLayout();
            toolPanel.SuspendLayout();
            SuspendLayout();
            // 
            // mainMenu
            // 
            mainMenu.Items.AddRange(new ToolStripItem[] { fileMenu, editMenu, viewMenu, codeMenu });
            mainMenu.Location = new Point(0, 0);
            mainMenu.Name = "mainMenu";
            mainMenu.Size = new Size(784, 24);
            mainMenu.TabIndex = 2;
            mainMenu.Text = "mainMenu";
            // 
            // fileMenu
            // 
            fileMenu.DropDownItems.AddRange(new ToolStripItem[] { openMenu, saveMenu, menuSeparator1, quitMenu });
            fileMenu.Name = "fileMenu";
            fileMenu.Size = new Size(37, 20);
            fileMenu.Text = "&File";
            // 
            // openMenu
            // 
            openMenu.Name = "openMenu";
            openMenu.ShortcutKeys = Keys.Control | Keys.O;
            openMenu.Size = new Size(146, 22);
            openMenu.Text = "&Open";
            openMenu.Click += openMenu_Click;
            // 
            // saveMenu
            // 
            saveMenu.Name = "saveMenu";
            saveMenu.ShortcutKeys = Keys.Control | Keys.S;
            saveMenu.Size = new Size(146, 22);
            saveMenu.Text = "&Save";
            saveMenu.Click += saveMenu_Click;
            // 
            // menuSeparator1
            // 
            menuSeparator1.Name = "menuSeparator1";
            menuSeparator1.Size = new Size(143, 6);
            // 
            // quitMenu
            // 
            quitMenu.Name = "quitMenu";
            quitMenu.ShortcutKeys = Keys.Control | Keys.Q;
            quitMenu.Size = new Size(146, 22);
            quitMenu.Text = "&Quit";
            quitMenu.Click += quitMenu_Click;
            // 
            // editMenu
            // 
            editMenu.Name = "editMenu";
            editMenu.Size = new Size(39, 20);
            editMenu.Text = "&Edit";
            // 
            // viewMenu
            // 
            viewMenu.DropDownItems.AddRange(new ToolStripItem[] { treeMenu });
            viewMenu.Name = "viewMenu";
            viewMenu.Size = new Size(44, 20);
            viewMenu.Text = "&View";
            // 
            // treeMenu
            // 
            treeMenu.Name = "treeMenu";
            treeMenu.ShortcutKeys = Keys.Control | Keys.T;
            treeMenu.Size = new Size(135, 22);
            treeMenu.Text = "&Tree";
            treeMenu.Click += treeMenu_Click;
            // 
            // codeMenu
            // 
            codeMenu.DropDownItems.AddRange(new ToolStripItem[] { runMenu });
            codeMenu.Name = "codeMenu";
            codeMenu.Size = new Size(47, 20);
            codeMenu.Text = "&Code";
            // 
            // runMenu
            // 
            runMenu.Name = "runMenu";
            runMenu.ShortcutKeys = Keys.Control | Keys.R;
            runMenu.Size = new Size(136, 22);
            runMenu.Text = "&Run";
            runMenu.Click += runMenu_Click;
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(0, 109);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(info);
            splitContainer1.Size = new Size(784, 492);
            splitContainer1.SplitterDistance = 407;
            splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            splitContainer2.Dock = DockStyle.Fill;
            splitContainer2.Location = new Point(0, 0);
            splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            splitContainer2.Panel1.Controls.Add(tree);
            // 
            // splitContainer2.Panel2
            // 
            splitContainer2.Panel2.Controls.Add(splitContainer3);
            splitContainer2.Size = new Size(784, 407);
            splitContainer2.SplitterDistance = 200;
            splitContainer2.TabIndex = 0;
            // 
            // tree
            // 
            tree.Dock = DockStyle.Fill;
            tree.Location = new Point(0, 0);
            tree.Name = "tree";
            tree.Size = new Size(200, 407);
            tree.TabIndex = 0;
            tree.AfterSelect += tree_AfterSelect;
            // 
            // splitContainer3
            // 
            splitContainer3.Dock = DockStyle.Fill;
            splitContainer3.Location = new Point(0, 0);
            splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            splitContainer3.Panel1.Controls.Add(tabs);
            splitContainer3.Panel1MinSize = 400;
            // 
            // splitContainer3.Panel2
            // 
            splitContainer3.Panel2.Controls.Add(prop);
            splitContainer3.Size = new Size(580, 407);
            splitContainer3.SplitterDistance = 400;
            splitContainer3.TabIndex = 0;
            // 
            // tabs
            // 
            tabs.Controls.Add(designerPage);
            tabs.Controls.Add(editorPage);
            tabs.Controls.Add(designSurfacePage);
            tabs.Dock = DockStyle.Fill;
            tabs.Location = new Point(0, 0);
            tabs.Name = "tabs";
            tabs.SelectedIndex = 0;
            tabs.Size = new Size(400, 407);
            tabs.TabIndex = 0;
            // 
            // designerPage
            // 
            designerPage.Controls.Add(pictureBox);
            designerPage.Location = new Point(4, 24);
            designerPage.Name = "designerPage";
            designerPage.Padding = new Padding(3);
            designerPage.Size = new Size(392, 379);
            designerPage.TabIndex = 0;
            designerPage.Text = "Designer";
            designerPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox
            // 
            pictureBox.BackColor = SystemColors.Info;
            pictureBox.Dock = DockStyle.Fill;
            pictureBox.Location = new Point(3, 3);
            pictureBox.Name = "pictureBox";
            pictureBox.Size = new Size(386, 373);
            pictureBox.TabIndex = 3;
            pictureBox.TabStop = false;
            pictureBox.SizeChanged += pictureBox_SizeChanged;
            pictureBox.DragDrop += pictureBox_DragDrop;
            pictureBox.DragEnter += pictureBox_DragEnter;
            pictureBox.MouseDown += pictureBox_MouseDown;
            pictureBox.MouseMove += pictureBox_MouseMove;
            pictureBox.MouseUp += pictureBox_MouseUp;
            // 
            // editorPage
            // 
            editorPage.Controls.Add(edit);
            editorPage.Location = new Point(4, 24);
            editorPage.Name = "editorPage";
            editorPage.Padding = new Padding(3);
            editorPage.Size = new Size(392, 379);
            editorPage.TabIndex = 1;
            editorPage.Text = "Editor";
            editorPage.UseVisualStyleBackColor = true;
            // 
            // edit
            // 
            edit.Dock = DockStyle.Fill;
            edit.Location = new Point(3, 3);
            edit.Multiline = true;
            edit.Name = "edit";
            edit.Size = new Size(386, 373);
            edit.TabIndex = 0;
            // 
            // designSurfacePage
            // 
            designSurfacePage.Location = new Point(4, 24);
            designSurfacePage.Name = "designSurfacePage";
            designSurfacePage.Padding = new Padding(3);
            designSurfacePage.Size = new Size(392, 379);
            designSurfacePage.TabIndex = 2;
            designSurfacePage.Text = "Design Surface";
            designSurfacePage.UseVisualStyleBackColor = true;
            // 
            // prop
            // 
            prop.Dock = DockStyle.Fill;
            prop.Location = new Point(0, 0);
            prop.Name = "prop";
            prop.Size = new Size(176, 407);
            prop.TabIndex = 0;
            // 
            // info
            // 
            info.Dock = DockStyle.Fill;
            info.Location = new Point(0, 0);
            info.Multiline = true;
            info.Name = "info";
            info.Size = new Size(784, 81);
            info.TabIndex = 0;
            // 
            // palette
            // 
            palette.Controls.Add(colorPage);
            palette.Controls.Add(componentPage);
            palette.Controls.Add(originalPage);
            palette.Dock = DockStyle.Top;
            palette.Location = new Point(0, 24);
            palette.Name = "palette";
            palette.SelectedIndex = 0;
            palette.Size = new Size(784, 85);
            palette.TabIndex = 4;
            // 
            // colorPage
            // 
            colorPage.Controls.Add(colorToolbar);
            colorPage.Location = new Point(4, 24);
            colorPage.Name = "colorPage";
            colorPage.Padding = new Padding(3);
            colorPage.Size = new Size(776, 57);
            colorPage.TabIndex = 0;
            colorPage.Text = "Colors";
            colorPage.UseVisualStyleBackColor = true;
            // 
            // colorToolbar
            // 
            colorToolbar.Location = new Point(3, 3);
            colorToolbar.Name = "colorToolbar";
            colorToolbar.Size = new Size(770, 25);
            colorToolbar.TabIndex = 0;
            colorToolbar.Text = "toolStrip1";
            // 
            // componentPage
            // 
            componentPage.Controls.Add(componentToolbar);
            componentPage.Location = new Point(4, 24);
            componentPage.Name = "componentPage";
            componentPage.Padding = new Padding(3);
            componentPage.Size = new Size(776, 57);
            componentPage.TabIndex = 1;
            componentPage.Text = "Components";
            componentPage.UseVisualStyleBackColor = true;
            // 
            // componentToolbar
            // 
            componentToolbar.Location = new Point(3, 3);
            componentToolbar.Name = "componentToolbar";
            componentToolbar.Size = new Size(770, 25);
            componentToolbar.TabIndex = 0;
            componentToolbar.Text = "toolStrip1";
            // 
            // originalPage
            // 
            originalPage.Controls.Add(toolPanel);
            originalPage.Location = new Point(4, 24);
            originalPage.Name = "originalPage";
            originalPage.Padding = new Padding(3);
            originalPage.Size = new Size(776, 57);
            originalPage.TabIndex = 2;
            originalPage.Text = "Original";
            originalPage.UseVisualStyleBackColor = true;
            // 
            // toolPanel
            // 
            toolPanel.Controls.Add(comboBox);
            toolPanel.Controls.Add(colorPanel);
            toolPanel.Dock = DockStyle.Fill;
            toolPanel.Location = new Point(3, 3);
            toolPanel.Name = "toolPanel";
            toolPanel.Size = new Size(770, 51);
            toolPanel.TabIndex = 0;
            // 
            // comboBox
            // 
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.FormattingEnabled = true;
            comboBox.Items.AddRange(new object[] { "line", "rectangle", "ellipse" });
            comboBox.Location = new Point(644, 5);
            comboBox.Name = "comboBox";
            comboBox.Size = new Size(121, 23);
            comboBox.TabIndex = 3;
            // 
            // colorPanel
            // 
            colorPanel.BackColor = Color.Blue;
            colorPanel.Location = new Point(5, 3);
            colorPanel.Name = "colorPanel";
            colorPanel.Size = new Size(54, 25);
            colorPanel.TabIndex = 2;
            // 
            // Window
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(784, 601);
            Controls.Add(splitContainer1);
            Controls.Add(palette);
            Controls.Add(mainMenu);
            MainMenuStrip = mainMenu;
            Name = "Window";
            Text = "Paint";
            mainMenu.ResumeLayout(false);
            mainMenu.PerformLayout();
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer2).EndInit();
            splitContainer2.ResumeLayout(false);
            splitContainer3.Panel1.ResumeLayout(false);
            splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer3).EndInit();
            splitContainer3.ResumeLayout(false);
            tabs.ResumeLayout(false);
            designerPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBox).EndInit();
            editorPage.ResumeLayout(false);
            editorPage.PerformLayout();
            palette.ResumeLayout(false);
            colorPage.ResumeLayout(false);
            colorPage.PerformLayout();
            componentPage.ResumeLayout(false);
            componentPage.PerformLayout();
            originalPage.ResumeLayout(false);
            toolPanel.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private MenuStrip mainMenu;
        private ToolStripMenuItem fileMenu;
        private ToolStripMenuItem openMenu;
        private ToolStripMenuItem saveMenu;
        private ToolStripSeparator menuSeparator1;
        private ToolStripMenuItem quitMenu;
        private SplitContainer splitContainer1;
        private SplitContainer splitContainer2;
        private TreeView tree;
        private SplitContainer splitContainer3;
        private PropertyGrid prop;
        private TextBox info;
        private TabControl palette;
        private TabPage colorPage;
        private TabPage componentPage;
        private TabControl tabs;
        private TabPage designerPage;
        private PictureBox pictureBox;
        private TabPage editorPage;
        private TextBox edit;
        private TabPage originalPage;
        private Panel toolPanel;
        private ComboBox comboBox;
        private Panel colorPanel;
        private ToolStrip colorToolbar;
        private ToolStrip componentToolbar;
        private ToolStripMenuItem editMenu;
        private ToolStripMenuItem viewMenu;
        private ToolStripMenuItem treeMenu;
        private ToolStripMenuItem codeMenu;
        private ToolStripMenuItem runMenu;
        private TabPage designSurfacePage;
    }
}
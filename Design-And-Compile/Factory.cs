﻿using System.ComponentModel;

namespace Designer
{
    public class Factory : ToolStripButton
    {
        private Type ComponentType;
        private string ComponentName;

        public Factory(Type type0)
        {
            ComponentType = type0;
            ComponentName = ComponentType.Name;

            this.Text = ComponentName;
            this.ToolTipText = ComponentName;

            // icon
            AttributeCollection attrCol = TypeDescriptor.GetAttributes (ComponentType);
            ToolboxBitmapAttribute imageAttr = attrCol [typeof (ToolboxBitmapAttribute)] as ToolboxBitmapAttribute;
            if (imageAttr != null)
            {
                this.Image = imageAttr.GetImage (ComponentType);
                this.Text = "";
            }

            this.MouseDown += this.Factory_MouseDown;
        }
        private void Factory_MouseDown(object sender, MouseEventArgs e)
        {
            DoDragDrop (this, DragDropEffects.All);
        }

        public object createInstance  ()
        {
            object result = Activator.CreateInstance (ComponentType);

            if (result is Box)
            {
                Box box = result as Box;
                box.Width = 80;
                box.Height = 40;
            }
            else if (result is Panel)
            {
                Panel p = result as Panel;
                p.Width = 80;
                p.Height = 40;
                p.BackColor = Color.CornflowerBlue;
                p.BorderStyle = BorderStyle.Fixed3D;
            }

            return result;
        }

    }
}

﻿using Builder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Designer
{
    public partial class Box : UserControl, ISelectedProperties
    {
        public TextBox info = null;
        List<string> ISelectedProperties.selectedProperites()
        { 
            return new List<string> { "BackColor" };  
        }

        public Box()
        {
            InitializeComponent();
            AllowDrop = true;
        }


        private int X0, Y0;
        private bool click = false;

        private void Box_MouseDown(object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void Box_MouseMove(object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Middle || Control.ModifierKeys == Keys.Shift)
                {
                    ColorDialog dlg = new ColorDialog();
                    dlg.Color = BackColor;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        BackColor = dlg.Color;
                    }
                }
                else if (e.Button == MouseButtons.Right || Control.ModifierKeys == Keys.Control)
                {
                    Width += e.X - X0;
                    Height += e.Y - Y0;

                    X0 = e.X;
                    Y0 = e.Y;
                }
                else if (e.Button == MouseButtons.Left)
                {
                    Left += e.X - X0;
                    this.Top = this.Top + e.Y - Y0;
                }
            }
        }

        private void Box_MouseUp(object sender, MouseEventArgs e)
        {
            click = false;
        }

        private void openMenu_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                BackgroundImage = new Bitmap(dlg.FileName);
            }
        }

        private void resizeMenu_Click(object sender, EventArgs e)
        {
            resizeMenu.Checked = !resizeMenu.Checked;
            if (resizeMenu.Checked)
                BackgroundImageLayout = ImageLayout.Stretch;
            else
                BackgroundImageLayout = ImageLayout.None;
        }

        private void Box_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(Color)))
                e.Effect = DragDropEffects.Move;
            else if (e.Data.GetDataPresent (DataFormats.Text))
                e.Effect = DragDropEffects.Copy;
            else if (e.Data.GetDataPresent(typeof (ToolStripItem)))
                e.Effect = DragDropEffects.Copy;
            // else
            //    e.Effect = DragDropEffects.None;

            /*
            if (info != null)
                foreach (var f in e.Data.GetFormats ())
                {
                    info.AppendText(f.ToString() + "\r\n");
                }
            */
        }

        private void Box_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(Color)))
            {
                BackColor = (Color) e.Data.GetData(typeof(Color));
            }
            else if (e.Data.GetDataPresent (DataFormats.Text))
            {
                string s = (string) e.Data.GetData(DataFormats.Text);

                Button btn = new Button();
                btn.Text = s;
                btn.Parent = this;

                Point location = PointToClient (new Point (e.X, e.Y));
                btn.Location = location;
            }
            else if (e.Data.GetDataPresent(typeof (ToolStripItem)))
            {
                ToolStripItem tool = e.Data.GetData (typeof (ToolStripItem)) as ToolStripItem;
                if (tool is Factory)
                {
                    Factory factory = tool as Factory;
                    Point location = PointToClient (new Point(e.X, e.Y));

                    Object obj = factory.createInstance ();
                    if (obj is Control)
                    {
                        Control ctl = obj as Control;
                        ctl.Location = location;
                        ctl.Parent = this;

                        new Adapter (ctl);
                    }
                }
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Designer
{
    public partial class ColorButton : ToolStripButton // UserControl
    {
        private Color color;

        public ColorButton (Color c, String name = "")
        {
            InitializeComponent ();
            color = c;

            const int size = 32;
            Bitmap img = new Bitmap (size, size);
            Graphics g = Graphics.FromImage (img);
            g.FillEllipse (new SolidBrush (color), 1, 1, size - 2, size - 2);

            this.Image = img;
            // this.Text = name;
            this.ToolTipText = name;

            this.MouseDown += ColorButton_MouseDown;
        }

        private void ColorButton_MouseDown (object sender, MouseEventArgs e)
        {
            DoDragDrop (color, DragDropEffects.All);
        }
    }
}

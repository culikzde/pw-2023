namespace Properties
{
    public partial class Window : Form
    {
        Pen pen;
        BrushProperties br;
        public Window()
        {
            InitializeComponent();
            pen = new Pen(Color.Red);
            br = new BrushProperties (Color.Blue, Color.Yellow);
            // propertyGrid1.SelectedObject = this;
            // propertyGrid1.SelectedObject = pen;
            propertyGrid1.SelectedObject = br;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class BrushProperties
    {
        public BrushProperties (Color first, Color second)
        {
            Color1 = first;
            Color2 = second;

            X1 = 0;
            Y1 = 0;

            X2 = 100;
            Y2 = 100;
        }

        public LinearGradientBrush createBrush()
        {
            return new LinearGradientBrush (new PointF(X1, Y1),
                                            new PointF(X2, Y2),
                                            Color1,
                                            Color2);
        }

        public Color Color1 { get; set; }

        public Color Color2 { get; set; }

        public int X1 { get; set; }

        public int Y1 { get; set; }

        public int X2 { get; set; }

        public int Y2 { get; set; }
    }
}

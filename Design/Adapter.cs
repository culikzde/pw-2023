﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http.Headers;

namespace Builder
{
    public partial class Adapter : UserControl
    {
        private Control comp;
        // private MainWindow win;

        public Adapter (Control c /*, MainWindow w */)
        {
            InitializeComponent ();
            comp = c;
            // win = w;
            comp.MouseDown += Adapter_MouseDown;
            comp.MouseMove += Adapter_MouseMove;
            comp.MouseUp += Adapter_MouseUp;
        }

        bool click = false;
        int X0, Y0;

        private void Adapter_MouseDown (object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
            if (e.Button == MouseButtons.Middle)
            {
                // win.SelectObject (comp);
                new Port (comp, 0, 0);
                new Port (comp, 0, 1);
                new Port (comp, 1, 0);
                new Port (comp, 1, 1);
            }
        }

        private void Adapter_MouseMove (object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Left)
                {
                    comp.Left += e.X - X0;
                    comp.Top += e.Y - Y0;
                    transfer ();
                }
                if (e.Button == MouseButtons.Right)
                {
                    comp.Width += e.X - X0;
                    comp.Height += e.Y - Y0;
                    X0 = e.X;
                    Y0 = e.Y;
                    transfer();
                }

            }
        }

        private void transfer ()
        {
            foreach (Control c in comp.Parent.Controls)
            {
                Port p = c as Port;
                if (p != null && p.comp == this.comp)
                {
                    p.place();
                }
            }
        }

        private void Adapter_MouseUp (object sender, MouseEventArgs e)
        {
            click = false;
        }

        private void Adapter_DragEnter (object sender, DragEventArgs e)
        {

        }

        private void Adapter_DragDrop (object sender, DragEventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic; // List
using System.ComponentModel; // ICustomTypeDescriptor

// https://www.codeproject.com/Articles/13342/Filtering-properties-in-a-PropertyGrid

namespace Designer
{ 
    public class ObjectWrapper : ICustomTypeDescriptor
    {
            private object obj = null;

            private List<string> selected_names = new List<string>() { "Name", "Text", "BackColor", "Size" };

        public ObjectWrapper (object param)
            {
                obj = param;
            }

            #region ICustomTypeDescriptor Members
            public PropertyDescriptorCollection GetProperties (Attribute[] attributes)
            {
                return GetProperties ();
            }

            public PropertyDescriptorCollection GetProperties ()
            {
               PropertyDescriptorCollection input = TypeDescriptor.GetProperties(obj);
            
              PropertyDescriptor[] outputArray = new PropertyDescriptor[0];
              PropertyDescriptorCollection output = new PropertyDescriptorCollection (outputArray, /* read only */ false);

              List<string> names = selected_names;

              if (obj is ISelectedProperties)
                 names = (obj as ISelectedProperties).selectedProperites();

              foreach (PropertyDescriptor item in input)
                if (names.Contains (item.Name))
                    output.Add(item);

               return output;
            }

            public AttributeCollection GetAttributes ()
            {
                return TypeDescriptor.GetAttributes (obj, true);
            }

            public String GetClassName ()
            {
                return TypeDescriptor.GetClassName (obj, true);
            }
            
            public String GetComponentName ()
            {
                return TypeDescriptor.GetComponentName (obj, true);
            }

            public TypeConverter GetConverter ()
            {
                return TypeDescriptor.GetConverter (obj, true);
            }

            public EventDescriptor GetDefaultEvent ()
            {
                return TypeDescriptor.GetDefaultEvent (obj, true);
            }

            public PropertyDescriptor GetDefaultProperty ()
            {
                return TypeDescriptor.GetDefaultProperty (obj, true);
            }

            public object GetEditor (Type editorBaseType)
            {
                return TypeDescriptor.GetEditor (this, editorBaseType, true);
            }

            public EventDescriptorCollection GetEvents (Attribute[] attributes)
            {
                return TypeDescriptor.GetEvents (obj, attributes, true);
            }

            public EventDescriptorCollection GetEvents ()
            {
                return TypeDescriptor.GetEvents (obj, true);
            }

            public object GetPropertyOwner (PropertyDescriptor pd)
            {
                return obj;
            }

            #endregion
        }
    }

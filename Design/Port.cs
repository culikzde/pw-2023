﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public partial class Port : UserControl
    {
        public Control comp;
        private int relx, rely;
        // public bool resize;

        public Port(Control c, int x, int y)
        {
            InitializeComponent();
            // Width = 8;
            // Height = 8;
            comp = c;
            relx = x;
            rely = y;

            place();
            Parent = comp.Parent;
            BringToFront();
        }

        private int X0, Y0;
        private bool click;

        private void MyPort_MouseDown(object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void MyPort_MouseMove(object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Left)
                {
                    if (false)
                    {
                        // move
                        Left += e.X - X0;
                        Top += e.Y - Y0;

                        comp.Left = Left - relx * (comp.Width - Width);
                        comp.Top = Top - rely * (comp.Height - Height);
                    }
                    else
                    {
                        // resize
                        int vx = e.X - X0;
                        int vy = e.Y - Y0;

                        if (relx == 0)
                        {
                            if (rely == 0)
                            {
                                comp.Left += vx;
                                comp.Top += vy;
                                comp.Width -= vx;
                                comp.Height -= vy;
                                X0 = e.X;
                                Y0 = e.Y;
                            }
                            else
                            {
                                comp.Left += vx;
                                comp.Height += vy;
                                comp.Width -= vx;
                                X0 = e.X;
                                Y0 = e.Y;
                            }
                        }
                        else
                        {
                            if (rely == 0)
                            {
                                comp.Top += vy;
                                comp.Height -= vy;
                                comp.Width += vx;
                                X0 = e.X;
                                Y0 = e.Y;
                            }
                            else
                            {
                                comp.Width += vx;
                                comp.Height += vy;
                                X0 = e.X;
                                Y0 = e.Y;
                            }
                        }
                    }
                    correct();
                }
            }
        }

        private void MyPort_MouseUp(object sender, MouseEventArgs e)
        {
            click = false;
        }


        public void place()
        {
            Left = comp.Left + relx * (comp.Width - Width);
            Top = comp.Top + rely * (comp.Height - Height);
        }

        public void correct()
        {
            foreach (Control c in Parent.Controls)
            {
                Port p = c as Port;
                if (p != null && p.comp == this.comp /* && p != this */)
                    p.place();
            }
        }

    }
}
